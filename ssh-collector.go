package main

import (
	"bufio"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/log"
	"gitlab.com/wobcom/ssh-exporter/config"
	"golang.org/x/crypto/ssh"
	"net"
	"strconv"
	"sync"
)

var (
	upDesc *prometheus.Desc
)

// SSHCollector collects metrics by regex matching the stdout of commands executed remotely via SSH
type SSHCollector struct {
	hosts []*config.HostConfig
}

func newSSHCollector(hosts []*config.HostConfig) *SSHCollector {
	return &SSHCollector{
		hosts: hosts,
	}
}

func init() {
	upDesc = prometheus.NewDesc("ssh_exporter_up", "1 if an SSH connection can be established", []string{"target"}, nil)
}

// Describe implements the collector.Collector interface's Describe function
func (s *SSHCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- upDesc
	for _, host := range s.hosts {
		for _, command := range host.Commands {
			for metricName, metric := range command.Metrics {
				staticLabels := map[string]string{
					"target":  host.HostName,
					"command": command.Command,
				}
				for labelName, labelValue := range metric.Labels {
					staticLabels[labelName] = labelValue
				}
                dynamicLabels := metric.DynamicLabels
                if metric.ValueAsLabel != "" {
                    dynamicLabels = append(dynamicLabels, metric.ValueAsLabel)
                }
				metric.Desc = prometheus.NewDesc(metricName, metric.Help, dynamicLabels, staticLabels)
				ch <- metric.Desc
			}
		}
	}
}

// Collect implements the collector.Collector interface's Collect function
func (s *SSHCollector) Collect(ch chan<- prometheus.Metric) {
	wg := &sync.WaitGroup{}
	wg.Add(len(s.hosts))
	for _, host := range s.hosts {
		host.Mutex.Lock()
		defer host.Mutex.Unlock()
		go s.collectForHost(ch, wg, host)
	}
	wg.Wait()
}

func (s *SSHCollector) collectForHost(ch chan<- prometheus.Metric, wg *sync.WaitGroup, host *config.HostConfig) {
	defer func() { wg.Done() }()
	sshClient, err := s.connect(host)
	defer sshClient.Close()
	if err != nil {
		ch <- prometheus.MustNewConstMetric(upDesc, prometheus.GaugeValue, 0, host.HostName)
		log.Error(err)
		return
	}

	ch <- prometheus.MustNewConstMetric(upDesc, prometheus.GaugeValue, 1, host.HostName)

	for _, command := range host.Commands {
		s.collectForCommand(ch, sshClient, host, command)
	}
}

func (s *SSHCollector) collectForCommand(ch chan<- prometheus.Metric, sshClient *ssh.Client, host *config.HostConfig, command *config.CommandConfig) {
	sshSession, err := sshClient.NewSession()
	if err != nil {
		log.Error(err)
		return
	}
	defer sshSession.Close()

	stdoutPipe, err := sshSession.StdoutPipe()
	scanner := bufio.NewScanner(stdoutPipe)
	err = sshSession.Run(command.Command)
	if err != nil {
		log.Error(err)
		return
	}
	for scanner.Scan() {
		line := scanner.Text()
		if command.Debug {
			log.Debugf("Scanned line from host '%s' for command '%s': '%s'", host.HostName, command.Command, line)
		}
		for _, metric := range command.Metrics {
			if matches := metric.RegexCompiled.FindStringSubmatch(line); matches != nil {
				if metric.DynamicLabels == nil {
                    createMetric(ch, metric, matches[1])
				} else {
					dynamicLabelValues := []string{}
					for _, dynamicLabelName := range metric.DynamicLabels {
						index := metric.RegexCompiled.SubexpIndex(dynamicLabelName)
						var dynamicLabelValue string
						if index < 0 {
							dynamicLabelValue = "!! match group not found !!"
						} else {
							dynamicLabelValue = matches[index]
						}
						dynamicLabelValues = append(dynamicLabelValues, dynamicLabelValue)
					}
					valueIndex := metric.RegexCompiled.SubexpIndex("value")
					if valueIndex < 0 {
						log.Errorf("Must define a value match group for metric value if using dynamic values")
					}
                    createMetric(ch, metric, matches[valueIndex], dynamicLabelValues...)

				}
			}
		}
	}
}

func createMetric(ch chan<- prometheus.Metric, metric *config.MetricConfig, value string, dynamicLabelValues ...string) {
    if metric.ValueAsLabel == "" {
        ch <- prometheus.MustNewConstMetric(metric.Desc, prometheus.GaugeValue, str2float64(value), dynamicLabelValues...)
        return
    } else if metric.ValueEnum == nil {
        ch <- prometheus.MustNewConstMetric(metric.Desc, prometheus.GaugeValue, 1, append(dynamicLabelValues, value)...)
    } else {
        for _, val := range metric.ValueEnum {
            ch <- prometheus.MustNewConstMetric(metric.Desc, prometheus.GaugeValue, boolToFloat64(val == value), append(dynamicLabelValues, val)...)
        }
    }
}

func (s *SSHCollector) connect(host *config.HostConfig) (*ssh.Client, error) {
	sshConfig, err := host.GetSSHConfig()
	if err != nil {
		return nil, err
	}

	return ssh.Dial("tcp", net.JoinHostPort(host.HostName, strconv.Itoa(host.Port)), sshConfig)
}

func str2float64(str string) float64 {
	value, err := strconv.ParseFloat(str, 64)
	if err != nil {
		log.Errorf("Could not parse '%s' as float!", str)
		return 0
	}
	return value
}

func boolToFloat64(b bool) float64 {
    if b {
        return 1
    } else {
        return 0
    }
}
