module gitlab.com/wobcom/ssh-exporter

go 1.15

require (
	github.com/prometheus/client_golang v1.7.1
	github.com/prometheus/common v0.13.0
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	gopkg.in/yaml.v2 v2.3.0
)
