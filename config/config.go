package config

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/log"
	"golang.org/x/crypto/ssh"
	"gopkg.in/yaml.v2"
	"io"
	"io/ioutil"
	"os"
	"regexp"
	"sync"
	"time"
)

const defaultConnectTimeout int = 5
const defaultCommandTimeout int = 20
const defaultPort int = 22

// Config stores the ssh-exporter's configuration
type Config struct {
	Hosts map[string]*HostConfig `yaml:"hosts,omitempty"`
}

// HostConfig stores host specifc configuration
type HostConfig struct {
	Mutex          sync.Mutex
	HostName       string
	Port           int              `yaml:"port,omitempty"`
	Username       string           `yaml:"username"`
	KeyFile        string           `yaml:"key_file,omitempty"`
	Password       string           `yaml:"password,omitempty"`
	ConnectTimeout int              `yaml:"connect_timeout,omitempty"`
	CommandTimeout int              `yaml:"command_timeout,omitempty"`
	Commands       []*CommandConfig `yaml:"commands"`
}

// CommandConfig stores command specific configuration
type CommandConfig struct {
	Command string                   `yaml:"command"`
	Debug   bool                     `yaml:"debug,omitempty"`
	Metrics map[string]*MetricConfig `yaml:"metrics"`
}

// MetricConfig stores metrics to extract by regex matching command's stdout
type MetricConfig struct {
	Regex         string `yaml:"regex"`
	RegexCompiled *regexp.Regexp
	Help          string            `yaml:"help,omitempty"`
	Labels        map[string]string `yaml:"labels,omitempty"`
	DynamicLabels []string          `yaml:"dynamic_labels,omitempty"`
    ValueAsLabel  string            `yaml:"value_as_label,omitempty"`
    ValueEnum     []string          `yaml:"value_enum,omitempty"`
	Desc          *prometheus.Desc
}

func newConfig() *Config {
	return &Config{
		Hosts: make(map[string]*HostConfig, 0),
	}
}

// Read reads a configuration from an io.Reader and returns either the Config or an error
func Read(reader io.Reader) (*Config, error) {
	content, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, err
	}

	config := newConfig()
	err = yaml.Unmarshal(content, config)
	if err != nil {
		return nil, err
	}

	for hostName, host := range config.Hosts {
		host.HostName = hostName
		if host.ConnectTimeout == 0 {
			host.ConnectTimeout = defaultConnectTimeout
		}
		if host.CommandTimeout == 0 {
			host.CommandTimeout = defaultCommandTimeout
		}
		if host.Port == 0 {
			host.Port = defaultPort
		}
		for _, command := range host.Commands {
			for metricName, metric := range command.Metrics {
				expr, err := regexp.Compile(metric.Regex)
				if err != nil {
					return nil, fmt.Errorf("Could not compile regex (host='%s', command='%s', metric='%s') '%s'", hostName, command.Command, metricName, metric.Regex)
				}
				metric.RegexCompiled = expr
			}
		}
	}
	return config, nil
}

// GetSSHAuthMethods returns a list of ssh.AuthMethod for a given host
func (host *HostConfig) GetSSHAuthMethods() ([]ssh.AuthMethod, error) {
	var authMethods []ssh.AuthMethod

	if host.KeyFile != "" {
		keyFile, err := os.Open(host.KeyFile)
		if err != nil {
			return nil, err
		}
		defer func() {
			if err = keyFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()

		keyFileContents, err := ioutil.ReadAll(keyFile)
		if err != nil {
			return nil, err
		}

		key, err := ssh.ParsePrivateKey(keyFileContents)
		if err != nil {
			return nil, err
		}

		authMethods = append(authMethods, ssh.PublicKeys(key))
	}

	if host.Password != "" {
		authMethods = append(authMethods, ssh.Password(host.Password))
	}

	if len(authMethods) == 0 {
		return nil, fmt.Errorf("I don't know how to authenticate with '%s'", host.HostName)
	}

	return authMethods, nil
}

// GetSSHConfig returns a ssh.ClientConfig for a given host
func (host *HostConfig) GetSSHConfig() (*ssh.ClientConfig, error) {
	authMethods, err := host.GetSSHAuthMethods()
	if err != nil {
		return nil, err
	}

	config := &ssh.ClientConfig{
		User:            host.Username,
		Auth:            authMethods,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Timeout:         time.Duration(host.ConnectTimeout) * time.Second,
	}
	config.SetDefaults()
	config.Ciphers = append(config.Ciphers, "diffie-hellman-group1-sha1")
	config.Ciphers = append(config.Ciphers, "diffie-hellman-group-exchange-sha1")

	return config, nil
}
