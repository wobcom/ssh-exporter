package main

import (
	"bufio"
	"flag"
	"fmt"
	"github.com/prometheus/common/log"
	"gitlab.com/wobcom/ssh-exporter/config"
	"net/http"
	"os"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const version string = "1.0.0"

var (
	showVersion   = flag.Bool("version", false, "Print version and exit")
	listenAddress = flag.String("web.listen-address", "[::]:9342", "Address to listen on")
	metricsPath   = flag.String("web.telemetry-path", "/metrics", "Path under which to expose metrics")
	configFile    = flag.String("config.file", "ssh-exporter.yml", "Configuration file")
	logLevel      = flag.String("log.level", "info", "Valid levels: [debug, info, warn, error, fatal]")
	configuration *config.Config
)

func main() {
	flag.Parse()

	err := log.Base().SetLevel(*logLevel)
	if err != nil {
		log.Fatal(err)
	}

	if *showVersion {
		printVersion()
		os.Exit(0)
	}

	err = initialize()
	if err != nil {
		log.Fatalf("Failed to initialize ssh-exporter: %v", err)
	}

	startServer()
}

func printVersion() {
	fmt.Println("ssh-exporter")
	fmt.Printf("version: %s\n", version)
	fmt.Println("Authors: @fluepke")
	fmt.Println("Scrape any metric from remote systems via SSH")
}

func initialize() error {
	err := loadConfiguration()
	if err != nil {
		return err
	}

	return nil
}

func loadConfiguration() error {
	log.Infof("Loading configuration from '%s'", *configFile)
	file, err := os.Open(*configFile)
	if err != nil {
		return err
	}
	defer func() {
		if err = file.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	reader := bufio.NewReader(file)
	configuration, err = config.Read(reader)
	if err != nil {
		return err
	}
	log.Infof("Loaded %d host(s) from configuration", len(configuration.Hosts))
	return nil
}

func startServer() {
	log.Infof("Starting ssh-exporter (vesion: %s)", version)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
            <head><title>ssh-exporter (Version ` + version + `)</title></head>
            <body>
            <h1>ssh-exporter</h1>
            <p><a href="` + *metricsPath + `">Metrics</a></p>
            </body>
            </html>`))
	})
	http.HandleFunc(*metricsPath, handleMetricsRequest)

	log.Infof("Listening on %s", *listenAddress)
	log.Fatal(http.ListenAndServe(*listenAddress, nil))
}

func handleMetricsRequest(w http.ResponseWriter, request *http.Request) {
	registry := prometheus.NewRegistry()

	var collector *SSHCollector

	if target := request.URL.Query().Get("target"); target != "" {
		host, found := configuration.Hosts[target]
		if !found {
			http.Error(w, "Target not configured", 404)
			return
		}
		collector = newSSHCollector([]*config.HostConfig{host})
	} else {
		hostList := []*config.HostConfig{}

		for _, host := range configuration.Hosts {
			hostList = append(hostList, host)
		}
		collector = newSSHCollector(hostList)
	}

	registry.MustRegister(collector)
	promhttp.HandlerFor(registry, promhttp.HandlerOpts{
		ErrorLog:      log.NewErrorLogger(),
		ErrorHandling: promhttp.ContinueOnError}).ServeHTTP(w, request)

}
