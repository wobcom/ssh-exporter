# Prometheus SSH Exporter
[![CI Status](https://gitlab.com/wobcom/ssh-exporter/badges/main/pipeline.svg)](https://gitlab.com/wobcom/ssh-exporter/pipelines) [![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/wobcom/ssh-exporter)](https://goreportcard.com/report/gitlab.com/wobcom/ssh-exporter)

This tool exposes metrics from remote hosts for [prometheus](https://github.com/prometheus/prometheus) by running commands remotely via SSH and regex-matching the stdout.

> **Note** Storing credentials on a networked computer for another computer causes security implications.

## Command line options
```
Usage of ./ssh-exporter:
  -config.file string
    	Configuration file (default "ssh-exporter.yml")
  -log.level string
    	Valid levels: [debug, info, warn, error, fatal] (default "info")
  -version
    	Print version and exit
  -web.listen-address string
    	Address to listen on (default "[::]:9342")
  -web.telemetry-path string
    	Path under which to expose metrics (default "/metrics")
```

## Example configuration
```yaml
---

hosts:
  example.com:
    username: monitoring
    password: example
    commands:
      - command: uptime
        debug: true  # displays all stdout read from remote host - set log.level to debug!
        metrics:
          load_avg_1min:
            help: Load average last 1 min 
            regex: "load average:\\s+(\\d+\\.\\d+)"
            labels:
              example: bar 
          load_avg_5min:
            regex: "load average:\\s+\\d+\\.\\d+,\\s+(\\d+\\.\\d+)"
```

results in the following metrics being reported

```
load_avg_1min{command="uptime",example="bar",target="example.com"} 23
load_avg_5min{command="uptime",target="example.com"} 42
ssh_exporter_up{target="example.com"} 1
```
